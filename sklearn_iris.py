import numpy
from sklearn.datasets import load_iris
import numpy as np
from scipy import linalg as la
import matplotlib.ticker
import matplotlib.pyplot as plt
import matplotlib
import pandas
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

iris = load_iris()
nData = len(iris.data)
redDim = 3

x_index = 3
y_index = 0


# label the color bar with the correct target name
formatter = matplotlib.ticker.FuncFormatter(lambda i, *args: iris.target_names[int(i)])
plt.figure(1)
plt.scatter(iris.data[:, x_index], iris.data[:, y_index], c=iris.target)
plt.colorbar(ticks=[0, 1, 2], format=formatter)
plt.xlabel(iris.feature_names[x_index])
plt.ylabel(iris.feature_names[y_index])

C = np.cov(np.transpose(iris.data))
classes = np.unique(iris.target_names)
sw = 0

for i in range(len(classes)):
    indices = np.squeeze(np.where(iris.target_names == classes))
    d = np.squeeze(iris.data[indices, :])
    classcov = np.cov(np.transpose(d))
    sw += np.float(np.shape(indices)[0])/nData * classcov

sb = C - sw

evals, evecs = la.eig(sw, sb)
print(evals, '\n')
print(evecs, '\n')
indices = np.argsort(evals)
indices = indices[::-1]

evecs = evecs[indices]
w = evecs[:, :redDim]

newData = np.dot(iris.data, w)
plt.figure(2)
plt.scatter(newData[:, 2], newData[:, 0], c=[iris.target])
#IRIS_LDA = open("IRIS_LDA.txt", "w")
np.savetxt('IRIS_LDA', newData)
print(newData[:, 1])
print(newData[:, 2])
#IRIS_LDA.close()
plt.show()
